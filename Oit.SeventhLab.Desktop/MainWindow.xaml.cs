﻿// ReSharper disable InconsistentNaming

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Oit.SeventhLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // constants from task
        private const double a = 1;
        private const double b = 4;
        private const double e = 0.01;
        private const int nx = 1000;
        private const int ny = 2;
        private const int np = 2;
        private const int nit = 10;

        // constants for drawing
        private const double maxX = 1000;
        private const double maxY = 500;
        private const double stepX = 0.5;
        private const double stepY = 10;
        private const double scaleX = 250;
        private const double scaleY = 10;

        // pen
        private static readonly Pen pen = new Pen(Color.Black, 1)
        {
            EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
            StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
        };


        // arrays we need
        private static List<double> ArrX { get; set; }
        private static List<double> ArrY1 { get; set; }
        private static List<double> ArrY2 { get; set; }
        private static List<double> ArrU1 { get; set; }
        private static List<double> ArrU2 { get; set; }

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs args)
        {
            InitializeArrays();
            ResultsTextBox.Clear();
            Alg(a, b, int.Parse(NxTextBox.Text), np, ny, nit, e, ResultsTextBox);
            DrawGraph();
        }


        // arrays initialization according to task
        private static void InitializeArrays()
        {
            ArrX = new List<double>();
            ArrY1 = new List<double>();
            ArrY2 = new List<double>();
            ArrU1 = new List<double>();
            ArrU2 = new List<double>();
        }

        // common algorithm
        private static double[] Alg(double a, double b, int nx, int np, int ny, int nit, double e, TextBox textBox)
        {
            var h = (b - a) / nx;
            var x = a;
            var u1 = 2 * a;
            var u2 = Math.Exp(a);
            var y = FPR(x, new double[] { u1, u2 }, ny);

            OUT(x, y, ny, textBox);

            for (var n = 0; n < nx; n++)
            {
                var f = FPR(x, y, ny);

                for (var i = 0; i < ny; i++)
                    y[i] = y[i] + h * f[i];

                x += h;

                if (n % np == 0)
                    OUT(x, y, ny, textBox);
            }

            return new double[1];
        }

        // data output
        private static void OUT(double x, double[] y, int ny, TextBox textBox)
        {
            ArrX.Add(x);
            ArrY1.Add(y[0]);
            ArrY2.Add(y[1]);
            ArrU1.Add(2 * x);
            ArrU2.Add(Math.Exp(x));
            textBox.Text += $"X = {x}\n";
            textBox.Text += $"Y1 = {y[0]}, U1 = {2 * x}, D1 = {Math.Abs(y[0] - 2 * x)}\n";
            textBox.Text += $"Y2 = {y[1]}, U1 = {Math.Exp(x)}, D2 = {Math.Abs(y[1] - Math.Exp(x))}\n\n";
        }

        // ???
        private static double[] FPR(double x, double[] y, int ny)
        {
            return new[]
            {
                y[0] / x + y[1] - Math.Exp(x),
                2 * x / y[0] + Math.Pow(y[1], 2) / Math.Exp(x) - 1
            };
        }

        // drawing graph
        private void DrawGraph()
        {
            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            graph.Clear(Color.White);

            DrawGrid(graph);
            DrawGraphLine(ArrX.ToArray(), ArrU1.ToArray(), graph, Color.Blue);
            DrawGraphLine(ArrX.ToArray(), ArrY1.ToArray(), graph, Color.DodgerBlue);
            DrawGraphLine(ArrX.ToArray(), ArrU2.ToArray(), graph, Color.Green);
            DrawGraphLine(ArrX.ToArray(), ArrY2.ToArray(), graph, Color.LightGreen);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // drawing grid
        private static void DrawGrid(Graphics graph)
        {
            pen.Width = 1;
            pen.Color = Color.Gray;
            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = Color.Red;
            graph.DrawLine(pen, 0, (float)(maxY - 10 * scaleY), (float)maxX, (float)(maxY - 10 * scaleY));
            graph.DrawLine(pen, 0, 0, 0, (float)maxY);
        }

        // drawing line
        private static void DrawGraphLine(double[] x, double[] y, Graphics graph, Color color)
        {
            var points = new PointF[x.Length];
            for (var i = 0; i < y.Length; i++)
            {
                if (y[i] > 0)
                    points[i] = new PointF((float)(x[i] * scaleX), (float)(maxY - y[i] * scaleY - 10 * scaleY));
                else if (y[i] < 0)
                    points[i] = new PointF((float)(x[i] * scaleX), (float)((-y[i] + 40) * scaleY));
                else
                    points[i] = new PointF((float)(x[i] * scaleX),(float)(40 * scaleY));
            }

            pen.Width = 2;
            pen.Color = color;
            graph.DrawCurve(pen, points);
        }

        // drawing point
        private static void DrawPoint(double x, double y, Graphics graph, Color color)
        {
            pen.Width = 10;
            pen.Color = color;
            graph.DrawEllipse(pen, (float)(maxX + x * scaleX), (float)(-y * scaleY), 1, 1);
        }
    }
}
