﻿// ReSharper disable InconsistentNaming
// ReSharper disable StringLiteralTypo

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oit.SecondLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DrawGraph();
        }

        // constants from task
        private const int n = 5;
        private const int maxIt = 900;
        private const double d = 4;
        private const double q = -2.25;
        private const double e = 0.0001;

        private static readonly double[,] ArrA =
        {
            { q, 1, 0, 0, 0 },
            { 1, -2, 1, 0, 0 },
            { 0, 1, -2, 1, 0 },
            { 0, 0, 1, -2, 1 },
            { 0, 0, 0, 1, q }
        };

        private static readonly double[] ArrB =
        {
            0, d, d, d, 0
        };

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs e)
        {
            var results = SI(ArrA, ArrB, n, MainWindow.e, double.Parse(RelaxTextBox.Text), maxIt);
            var resultsStr = string.Empty;

            foreach (var result in results)
            {
                resultsStr += $"Итерация {result.Item1}: ";
                resultsStr = result.Item3.Aggregate(resultsStr, (current, value) => current + $"{value} ");
                resultsStr += "\n";
            }

            ResultsTextBox.Text = resultsStr;
        }

        // on Draw Graph Button click
        private void DrawGraph()
        {
            const double maxX = 1000;
            const double maxY = 500;
            const double stepX = 0.1;
            const double stepY = 100;
            const double scaleX = 500;
            const double scaleY = 0.5;

            var gridColor = Color.Gray;
            var axisColor = Color.Red;
            var graphColor = Color.Blue;

            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            var pen = new Pen(gridColor, 1)
            {
                EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
                StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
            };

            graph.Clear(Color.White);

            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = axisColor;
            graph.DrawLine(pen, 0, (float)maxY - 1, (float)maxX, (float)maxY - 1);
            graph.DrawLine(pen, 0, 0, 0, (float)maxY);


            var pointsList = new List<Tuple<float, float>>();
            for (var w = 0.1; w < 2; w += 0.1)
            {
                var lastResult = SI(ArrA, ArrB, n, MainWindow.e, w, maxIt).Last();
                pointsList.Add(new Tuple<float, float>((float)w, lastResult.Item1));
            }

            var points = new PointF[pointsList.Count];
            for (var i = 0; i < pointsList.Count; i++)
                points[i] = new PointF((float)(pointsList[i].Item1 * scaleX), (float)(maxY - pointsList[i].Item2 * scaleY));


            pen.Width = 2;
            pen.Color = graphColor;
            graph.DrawCurve(pen, points);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // simple iteration method implementation
        private static IEnumerable<Tuple<int, double, IEnumerable<double>>> SI(double[,] a, double[] b, int n, double e, double w, int maxIteration)
        {
            var x = new double[n];
            var xo = new double[n];

            for (var i = 0; i < n; i++)
                x[i] = xo[i] = 1;

            var results = new List<Tuple<int, double, IEnumerable<double>>>();
            var iteration = 0;

            double delta;
            do
            {
                delta = 0;

                for (var i = 0; i < n; i++)
                {
                    var s = b[i];

                    for (var j = 0; j < n; j++)
                        if (i != j)
                            s = s - a[i, j] * xo[j];

                    s = s / a[i, i];
                    s = w * s + (1 - w) * x[i];

                    var d = Math.Abs(x[i] - s);
                    x[i] = s;

                    if (d > delta)
                        delta = d;
                }

                iteration++;

                for (var i = 0; i < n; i++)
                    xo[i] = x[i];

                results.Add(new Tuple<int, double, IEnumerable<double>>(iteration, delta, x.ToList()));

            } while (iteration < maxIteration && delta > e);

            return results;
        }
    }
}
