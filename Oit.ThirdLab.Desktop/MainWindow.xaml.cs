﻿// ReSharper disable InconsistentNaming

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oit.ThirdLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // constants from task
        private const double a = 0;
        private const double b = 4;
        private const int n = 21;
        private const int m = 21;

        private double[] ArrX { get; set; }
        private double[] ArrY { get; set; }
        private double[] ArrZ { get; set; }
        private double[] ArrXTest { get; set; }

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs e)
        {
            InitializeArrays();

            var resultStr = string.Empty;
            for (var i = 1; i < ArrX.Length - 1; i++)
            {
                var y = F(ArrX[i]);
                var z = PNS(new[] {ArrX[i - 1], ArrX[i], ArrX[i + 1]}, new[] { ArrY[i - 1], ArrY[i], ArrY[i + 1] }, m, ArrX[i]);
                var d = Math.Abs(y - z);

                ArrZ[i] = z;
                resultStr += $"X = {ArrX[i]}, Y = {y}, Z = {z}, D = {d} \n";
            }

            ResultsTextBox.Text = resultStr;
            DrawGraph();
        }

        // arrays initialization according to task
        private void InitializeArrays()
        {
            ArrX = new double[m];
            for (var i = 1; i < m + 1; i++)
                ArrX[i - 1] = a + (i - 1) * (b - a) / (m - 1);

            ArrY = new double[m];
            for (var i = 0; i < m; i++)
                ArrY[i] = F(ArrX[i]);
            
            ArrXTest = new double[n];
            for (var i = 0; i < n; i++)
                ArrXTest[i] = (b - a) * i / 20;

            ArrZ = new double[m];
        }

        // draw graph from test array
        private void DrawGraph()
        {
            const double maxX = 1000;
            const double maxY = 500;
            const double stepX = 0.5;
            const double stepY = 0.5;
            const double scaleX = 250;
            const double scaleY = 100;

            var gridColor = Color.Gray;
            var axisColor = Color.Red;
            var sourceGraphColor = Color.Blue;
            var interpolationGraphColor = Color.Green;

            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            var pen = new Pen(gridColor, 1)
            {
                EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
                StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
            };

            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = axisColor;
            graph.DrawLine(pen, 0, (float)maxY / 2, (float)maxX, (float)maxY / 2);
            graph.DrawLine(pen, 0, 0, 0, (float)maxY);

            var pointsListY = new List<Tuple<float, float>>();
            var pointsListZ = new List<Tuple<float, float>>();

            for (var i = 0; i < ArrXTest.Length; i++)
            {
                var x = ArrXTest[i];
                pointsListY.Add(new Tuple<float, float>((float) x, (float) F(x)));
                pointsListZ.Add(new Tuple<float, float>((float) x, (float) ArrZ[i]));
            }

            var pointsY = new PointF[pointsListY.Count];
            var pointsZ = new PointF[pointsListZ.Count];

            for (var i = 0; i < pointsListY.Count; i++)
                pointsY[i] = new PointF((float)(pointsListY[i].Item1 * scaleX), (float)(maxY / 2 - pointsListY[i].Item2 * scaleY));

            for (var i = 0; i < pointsListZ.Count; i++)
                pointsZ[i] = new PointF((float)(pointsListZ[i].Item1 * scaleX), (float)(maxY / 2 - pointsListZ[i].Item2 * scaleY));

            pen.Width = 2;
            pen.Color = sourceGraphColor;
            graph.DrawCurve(pen, pointsY);

            pen.Color = interpolationGraphColor;
            graph.DrawCurve(pen, pointsZ);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // source function
        private static double F(double x)
        {
            return Math.Sin(x) * Math.Sin(x) - x / 5;
        }

        // calculation PNS
        private static double PNS(double[] x, double[] y, int m, double xt)
        {
            if (xt < x[0] && xt > x[m - 1])
                return double.NaN;

            var i = 2;
            while (xt > x[i])
                i++;

            return N2(x, y, xt);
        }

        // calculation N2
        private static double N2(double[] x, double[] y, double xt)
        {
            var delta = DLT(x, y, 1, 1);
            var delta2 = DLT(x, y, 1, 2);

            return y[0] + delta * xt - delta * x[0] +
                   delta2 * xt * xt - delta2 * x[0] * xt - delta2 * x[1] * xt + delta2 * x[0] * x[1];
        }

        // recursive delta calculation
        private static double DLT(double[] x, double[] y, int i, int k)
        {
            if (k == 1)
                return (y[i - 1] - y[i]) / (x[i - 1] - x[i]);
            return (DLT(x, y, i, k - 1) - DLT(x, y, i + 1, k - 1)) / (x[i - 1] - x[i - 1 + k]);
        }
    }
}