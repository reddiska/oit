﻿// ReSharper disable InconsistentNaming
// ReSharper disable StringLiteralTypo

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oit.FifthLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // constants from task
        private const int m = 41;
        private const double a = -4;
        private const double b = 0;
        private const double e = 0.0001;
        private const double maxIteration = 100;

        // constants for drawing
        private const double maxX = 1000;
        private const double maxY = 500;
        private const double stepX = 0.5;
        private const double stepY = 0.5;
        private const double scaleX = 250;
        private const double scaleY = 100;

        // pen
        private static readonly Pen pen = new Pen(Color.Black, 1)
        {
            EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
            StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
        };

        // arrays we need to draw graphs
        private static double[] ArrX { get; set; }
        private static double[] ArrY { get; set; }

        // approximate points
        private static double ArrXApp { get; set; }
        private static double ArrYApp { get; set; }

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs args)
        {
            InitializeArrays();

            var results = new List<Tuple<int, double, double>>();
            var x = MNRe(double.Parse(FirstApproximationTextBox.Text), e, results);

            ResultsTextBox.Clear();
            ResultsTextBox.Text += $"Начальное приближение = {x}\n-----\n";
            foreach (var result in results)
                ResultsTextBox.Text += $"Итерация {result.Item1 + 1}: X* = {result.Item2}, D = {result.Item3}, Y* = {F(result.Item2)}\n";
            ResultsTextBox.Text += $"\n";

            ArrXApp = results.Last().Item2;
            ArrYApp = F(results.Last().Item2);

            DrawGraph();
        }

        // arrays initialization according to task
        private static void InitializeArrays()
        {
            var h = (Math.Abs(b - a) / (m - 1));

            ArrX = new double[m];
            for (var i = 0; i < m; i++)
                ArrX[i] = a + h * i;

            ArrY = new double[m];
            for (var i = 0; i < m; i++)
                ArrY[i] = F(ArrX[i]);
        }

        // drawing graph
        private void DrawGraph()
        {
            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            graph.Clear(Color.White);

            DrawGrid(graph);
            DrawGraphLine(ArrX, ArrY, graph, Color.Green);
            DrawPoint(ArrXApp, ArrYApp, graph, Color.Blue);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // drawing grid
        private static void DrawGrid(Graphics graph)
        {
            pen.Width = 1;
            pen.Color = Color.Gray;
            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = Color.Red;
            graph.DrawLine(pen, 0, (float)maxY / 2, (float)maxX, (float)maxY / 2);
            graph.DrawLine(pen, (float)maxX - 1, 0, (float)maxX - 1, (float)maxY);
        }

        // drawing line
        private static void DrawGraphLine(double[] x, double[] y, Graphics graph, Color color)
        {
            var points = new PointF[x.Length];
            for (var i = 0; i < y.Length; i++)
                points[i] = new PointF((float)(maxX + x[i] * scaleX), (float)(maxY / 2 - y[i] * scaleY));

            pen.Width = 2;
            pen.Color = color;
            graph.DrawCurve(pen, points);
        }

        // drawing point
        private static void DrawPoint(double x, double y, Graphics graph, Color color)
        {
            pen.Width = 10;
            pen.Color = color;
            graph.DrawEllipse(pen, (float)(maxX + x * scaleX), (float)(maxY / 2 - y * scaleY), 1, 1);
        }

        // source function
        private static double F(double x)
        {
            return Math.Pow(Math.Sin(x), 2) - x / 5 - 1;
        }

        // first derivative of the source function
        private static double FDAcc(double x)
        {
            return 2 * Math.Sin(x) * Math.Cos(x) - 0.2;
        }

        // returns next approximation
        private static double MN(double x)
        {
            return x - F(x) / FDAcc(x);
        }

        // running MN recursive
        private static double MNRe(double x, double e, List<Tuple<int, double, double>> results, int iteration = 0)
        {
            var next = MN(x);
            results.Add(new Tuple<int, double, double>(iteration, x, Math.Abs(x - next)));
            return Math.Abs(F(x)) > e && iteration < maxIteration ? MNRe(next, e, results, ++iteration) : next;
        }
    }
}