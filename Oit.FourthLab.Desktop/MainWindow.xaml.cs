﻿// ReSharper disable InconsistentNaming
// ReSharper disable StringLiteralTypo

using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oit.FourthLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // constants from task
        private const int m = 11;
        private const double a = 0;
        private const double b = 4;

        // variables from task
        private double step = 0.05; // can be 0.2, 0.1 or 0.05
        private double number = 50; // can be 10, 20 or 50

        // constants for drawing
        private const double maxX = 1000;
        private const double maxY = 500;
        private const double stepX = 0.5;
        private const double stepY = 0.5;
        private const double scaleX = 250;
        private const double scaleY = 100;

        // pen
        private static readonly Pen pen = new Pen(Color.Black, 1)
        {
            EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
            StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
        };

        // arrays that we need to draw graphs
        private static double[] ArrX { get; set; }
        private static double[] ArrY { get; set; }
        private static double[] ArrFdAcc { get; set; }
        private static double[] ArrFDApp { get; set; }
        private static double[] ArrSDAcc { get; set; }
        private static double[] ArrSDApp { get; set; }

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs e)
        {
            if (Step01.IsChecked == true)
                step = 0.1;
            if (Step02.IsChecked == true)
                step = 0.2;
            if (Step005.IsChecked == true)
                step = 0.05;

            if (Number10.IsChecked == true)
                number = 10;
            if (Number20.IsChecked == true)
                number = 20;
            if (Number50.IsChecked == true)
                number = 50;

            InitializeArrays();
            DrawGraph();

            ResultsTextBox.Text = $"Значение интеграла: {I(a, b, number)}";
        }

        // drawing graph
        private void DrawGraph()
        {
            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            graph.Clear(Color.White);

            DrawGrid(graph);
            DrawGraphLine(ArrX, ArrY, graph, Color.BlueViolet);
            DrawGraphLine(ArrX, ArrFdAcc, graph, Color.Green);
            DrawGraphLine(ArrX, ArrFDApp, graph, Color.LightGreen);
            DrawGraphLine(ArrX, ArrSDAcc, graph, Color.Blue);
            DrawGraphLine(ArrX, ArrSDApp, graph, Color.LightSkyBlue);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // drawing grid
        private static void DrawGrid(Graphics graph)
        {
            pen.Width = 1;
            pen.Color = Color.Gray;
            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = Color.Red;
            graph.DrawLine(pen, 0, (float)maxY / 2, (float)maxX, (float)maxY / 2);
            graph.DrawLine(pen, 0, 0, 0, (float)maxY);
        }

        // drawing line
        private static void DrawGraphLine(double[] x, double[] y, Graphics graph, Color color)
        {
            var points = new PointF[ArrX.Length];
            for (var i = 0; i < ArrX.Length; i++)
                points[i] = new PointF((float) (x[i] * scaleX), (float) (maxY / 2 - y[i] * scaleY));

            pen.Width = 2;
            pen.Color = color;
            graph.DrawCurve(pen, points);
        }

        // arrays initialization according to task
        private void InitializeArrays()
        {
            ArrX = new double[m];
            for (var i = 1; i < m + 1; i++)
                ArrX[i - 1] = a + (i - 1) * (b - a) / (m - 1);

            ArrY = new double[m];
            for (var i = 0; i < m; i++)
                ArrY[i] = F(ArrX[i]);

            ArrFdAcc = new double[m];
            for (var i = 0; i < m; i++)
                ArrFdAcc[i] = FDAcc(ArrX[i]);

            ArrFDApp = new double[m];
            for (var i = 1; i < m - 1; i++)
                ArrFDApp[i] = FDApp(ArrX[i], step);

            ArrFDApp[0] = FDAppA(a, step);
            ArrFDApp[m - 1] = FDAppB(b, step);

            ArrSDAcc = new double[m];
            for (var i = 0; i < m; i++)
                ArrSDAcc[i] = SDAcc(ArrX[i]);

            ArrSDApp = new double[m];
            for (var i = 1; i < m - 1; i++)
                ArrSDApp[i] = SDApp(ArrX[i], step);
        }

        // source function
        private static double F(double x)
        {
            return Math.Sin(x) * Math.Sin(x) - x / 5;
        }

        // accurate first derivative of the source function
        private static double FDAcc(double x)
        {
            return 2 * Math.Sin(x) * Math.Cos(x) - 0.2;
        }

        // accurate second derivative of the source function
        private static double SDAcc(double x)
        {
            return -2 * Math.Pow(Math.Sin(x), 2) + Math.Pow(Math.Cos(x), 2);
        }

        // calculating first derivative for start point of interval
        private static double FDAppA(double x, double step)
        {
            return (3 * F(x) - 4 * F(x + step) + F(x + step * 2)) / 2 * step;
        }

        // calculating first derivative for end point of interval
        private static double FDAppB(double x, double step)
        {
            return (F(x - step * 20) - 4 * F(x - step) + 3 * F(x)) / 2 * step;
        }

        // calculating first derivative via simple formula
        private static double FDApp(double x, double step)
        {
            return (F(x + step) - F(x - step)) / (2 * step);
        }

        // calculating second derivative via simple formula
        private static double SDApp(double x, double step)
        {
            return (F(x - step) - 2 * F(x) + F(x + step)) / Math.Pow(step, 2);
        }

        // calculating integral via trapezium method
        private static double I(double a, double b, double n)
        {
            var h = (b - a) / n;
            var sum = (F(a) + F(b)) / 2;

            for (var x = a + h; x < b; x += h)
                sum += F(x);

            return sum * h;
        }
    }
}