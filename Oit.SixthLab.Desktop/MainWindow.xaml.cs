﻿// ReSharper disable InconsistentNaming

using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oit.SixthLab.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // constants from task
        private const int m = 41;
        private const double a = -9;
        private const double b = -1;
        private const double e = 0.0001;
        private const double maxIteration = 100;

        // constants for drawing
        private const double maxX = 1000;
        private const double maxY = 500;
        private const double stepX = 1;
        private const double stepY = 50;
        private const double scaleX = 100;
        private const double scaleY = 2;

        // pen
        private static readonly Pen pen = new Pen(Color.Black, 1)
        {
            EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor,
            StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor
        };

        // arrays we need
        private static double[] ArrX { get; set; }
        private static double[] ArrY { get; set; }

        private static double X { get; set; }
        private static double Y { get; set; }
        private static double I { get; set; }

        // on Run Calculation Button click
        private void RunCalculation(object sender, RoutedEventArgs args)
        {
            InitializeArrays();

            ResultsTextBox.Clear();
            for (var i = 0; i < ArrX.Length; i++)
                ResultsTextBox.Text += $"X = {ArrX[i]}, Y = {ArrY[i]}\n";
            ResultsTextBox.Text += "\n";

            var result = MPP(double.Parse(FirstApproximationTextBox.Text), double.Parse(StepTextBox.Text), e);

            I = result.Item1;
            X = result.Item2;
            Y = F(X);

            ResultsTextBox.Text += $"#{I}: X(min) = {X}, Y(min) = {Y}";
            DrawGraph();
        }

        // arrays initialization according to task
        private static void InitializeArrays()
        {
            var h = (Math.Abs(b - a) / (m - 1));

            ArrX = new double[m];
            for (var i = 0; i < m; i++)
                ArrX[i] = a + h * i;

            ArrY = new double[m];
            for (var i = 0; i < m; i++)
                ArrY[i] = F(ArrX[i]);
        }

        // drawing graph
        private void DrawGraph()
        {
            var bmp = new Bitmap((int)maxX, (int)maxY);
            var graph = Graphics.FromImage(bmp);

            graph.Clear(Color.White);

            DrawGrid(graph);
            DrawGraphLine(ArrX, ArrY, graph, Color.Green);
            DrawPoint(X, Y, graph, Color.Blue);

            GraphImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
        }

        // drawing grid
        private static void DrawGrid(Graphics graph)
        {
            pen.Width = 1;
            pen.Color = Color.Gray;
            graph.DrawRectangle(pen, 0, 0, (float)(maxX) - 1, (float)(maxY - 1));

            for (var i = stepX * scaleX; i < maxX; i += stepX * scaleX)
                graph.DrawLine(pen, (float)i, 0, (float)i, (float)(maxY - 1));

            for (var i = stepY * scaleY; i < maxY; i += stepY * scaleY)
                graph.DrawLine(pen, 0, (float)i, (float)(maxX - 1), (float)i);

            pen.Color = Color.Red;
            graph.DrawLine(pen, 0, 0, (float)maxX, 0);
            graph.DrawLine(pen, (float)maxX - 1, 0, (float)maxX - 1, (float)maxY);
        }

        // drawing line
        private static void DrawGraphLine(double[] x, double[] y, Graphics graph, Color color)
        {
            var points = new PointF[x.Length];
            for (var i = 0; i < y.Length; i++)
                points[i] = new PointF((float)(maxX + x[i] * scaleX), (float)(-y[i] * scaleY));

            pen.Width = 2;
            pen.Color = color;
            graph.DrawCurve(pen, points);
        }

        // drawing point
        private static void DrawPoint(double x, double y, Graphics graph, Color color)
        {
            pen.Width = 10;
            pen.Color = color;
            graph.DrawEllipse(pen, (float)(maxX + x * scaleX), (float)(-y * scaleY), 1, 1);
        }

        // source function
        private static double F(double x)
        {
            return 20 * x * Math.Pow(Math.Sin(x), 2) - Math.Pow(x, 2);
        }

        // brute force method implementation
        private static Tuple<int, double> MPP(double xo, double h, double e)
        {
            var i = 0;
            var x = xo;
            var y = F(x);

            do
            {
                i++;

                var yo = y;

                xo = x;
                x = xo + h;
                y = F(x);

                if (y <= yo)
                    continue;

                h = -h / 4;
            } while (Math.Abs(h) > e / 4);

            return new Tuple<int, double>(i, xo);
        }
    }
}